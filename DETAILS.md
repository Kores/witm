
## The problem

Nowadays, we have a lot of diversity in the package management area, we have the greatest package managers, like:

- [pacman](https://wiki.archlinux.org/title/pacman) (& [yay](https://github.com/Jguer/yay) & [paru](https://github.com/Morganamilo/paru))
- [ports & pkg](https://docs.freebsd.org/en/books/handbook/ports/)
- [nix](https://nixos.org/) (also provides reproducible builds and promotes source distribution)
- [apk](https://wiki.alpinelinux.org/wiki/Package_management)
- [zypper](https://en.opensuse.org/Portal:Zypper)
- [xbps](https://docs.voidlinux.org/xbps/index.html)
- [flatpak](https://flatpak.org/) (which is also a runtime and oriented for binary distribution)
- [eopkg](https://getsol.us/articles/package-management/basics/en/)
- [Portage](https://wiki.gentoo.org/wiki/Portage)
- [Entropy](https://sabayon.github.io/wiki-next/articles/entropy/) (~~I feel sad about this one, seems abandoned~~)
- [cargo](https://github.com/rust-lang/cargo) (& [cargo-update](https://github.com/nabijaczleweli/cargo-update)).
- [gem](https://guides.rubygems.org/command-reference/)
- [opkg](https://openwrt.org/docs/guide-user/additional-software/opkg)
- [Nimble](https://nimble.directory/)
- [hex](https://hex.pm/)
- [zigmod](https://github.com/nektro/zigmod)
- ...

The decent ones:

- [apt](https://wiki.debian.org/AptCLI)
- [yum](https://wiki.centos.org/PackageManagement/Yum)
- [dnf](https://docs.fedoraproject.org/en-US/quick-docs/dnf/)
- [pip](https://pip.pypa.io/en/stable/)
- [npm](https://docs.npmjs.com/about-npm)
- [Homebrew](https://brew.sh/)
- [Chocolatey](https://chocolatey.org/)
- [Scoop](https://scoop.sh/)
- ...

The almost decent ones:

- [sdkman](https://sdkman.io/)
- [docker](https://docker.com/) (sort of)
- ...

And the bad ones:

- [snap](https://snapcraft.io/)
- No, there is no other one, it is just snap.

*They are not sorted in order of preference.*

Okay, it's just my personal opinion (I'm lying, no, it's not, [snap](https://snapcraft.io/) is really the trashier one, the others are miles away better),
but you get it, we have a lot of ways to install ~~malware~~ software in our machines.

However, I always feel very uncomfortable with the fact that, it's not always that we can easily find a manpage for a package
installed in a non-standard way, for example, if you install a package with [npm](https://docs.npmjs.com/about-npm)
(which the packages rarely ships any manpages whatsoever), [cargo¹](https://github.com/rust-lang/cargo) or simply
by downloading its binary, you wouldn't be able to read the manpages with `man`.

There are some alternatives, like, if they are in fact shipped with manpages but the package manager just don't support
automatic installation (like [cargo¹](https://github.com/rust-lang/cargo) and [snap~~trash~~](https://snapcraft.io/)),
one can simply specify a custom directory for manpages.

> ¹: Cargo is not meant to be a way for end-users to install packages in their system, although it's very
> common for devs to use it to install some crates, in cases where they are not available in the distro repository,
> or when they need special features not enabled by default (or any other reason, like, just because they can).

