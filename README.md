# Where Is The Manual

**Note, this application still under work, this is being published early just for name reservation.**

Have you ever wondered where the manual is? I can find it for you, I hope.

## What it does?

**witm** is a tool to access online and offline manpages and info pages.

## Installation

### Nix (recommended)

```shell
nix-env -f https://gitlab.com/Kores/witm/-/archive/main/witm-main.tar.bz2 -iA witm
```

### Cargo (not recommended)

```shell
cargo install witm
```

## Usage

```shell
witm ripgrep
```

By default, **witm** will try to find the manpage from your distro provided manpages site, you can specify
which one to use with the `-p` flag.

```shell
witm rg -p freebsd
```
