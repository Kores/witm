use clap::{ArgEnum, Args, Parser, Subcommand};

#[derive(Parser, Debug)]
#[clap(author, version, about)]
pub(crate) struct Opts {
    #[clap(
        required = true,
        min_values = 1,
        max_values = 2,
        value_name = "[section] page"
    )]
    pub(crate) section: Vec<String>,

    #[clap(arg_enum, required = false, short, long, default_value = "arch")]
    pub(crate) provider: Provider,
}

// TODO: there should be a way to query if the provider supports specifying a lnguage, so the user
// TODO: is not tricked into thinking that the option is not working.
#[derive(ArgEnum, Copy, Clone, Debug)]
pub(crate) enum Provider {
    ArchLinux,
    ArchLinuxAny,
    FreeBSD,
}

struct ManPageRequest {
    package: Option<String>,
    page: String,
    section: Option<String>,
    lang: Option<String>,
}

impl ManPageRequest {
    fn package(&self) -> Option<&str> {
        self.package.as_ref().map(|s| s.as_str())
    }

    fn page(&self) -> &str {
        &self.page
    }

    fn section(&self) -> Option<&str> {
        self.section.as_ref().map(|s| s.as_str())
    }

    fn lang(&self) -> Option<&str> {
        self.lang.as_ref().map(|s| s.as_str())
    }
}

trait IntoProviderUrl {
    fn into_provider_urls(self, request: ManPageRequest) -> Vec<String>;
}

impl IntoProviderUrl for Provider {
    /// Convert [Provider] to a list of urls where you can find the manpages, commonly its a single url,
    /// but we may want to scan multiple urls for the manpage, in cases where the server doesn't support
    /// a way to omit this value.
    ///
    /// The API is encouraged to cache the results locally, also, *witm* will be shipped with a compressed
    /// cache for bultin manpage providers, so we will know the correct url to prioritize.
    ///
    /// ## Details
    ///
    /// ### Arch Linux
    ///
    /// `tpl`:`https://man.archlinux.org/man/{repository}/{package}/{page}.{section}.{lang}.txt`
    ///
    /// By the Arch Linux man site documentation, which you can find [here](https://man.archlinux.org/),
    /// we can omit anything but `{page}`, here we selectively omit `{package}`, `{section}` and `{lang}`
    /// whenever those values are not available.
    ///
    /// The alternative ArchLinuxAny will be removed once we test and confirm the caching is working,
    /// this choice will also be moved to a custom provider added only for testing purposes, and
    /// the testing suite MUST NEVER call Arch Linux sites, AUR already struggles with some tools
    /// doing heavy indexing and bringing their site down, we should never be a culpit.
    ///
    fn into_provider_urls(self, request: ManPageRequest) -> Vec<String> {
        match self {
            Provider::ArchLinux => vec![
                format!(
                    "https://man.archlinux.org/man{slash_package}/{page}{dot_section}{dot_lang}.txt",
                    slash_package = request.package().map(|s| format!("/{}", s)).unwrap_or_default(),
                    page = request.page(),
                    dot_section = request.section().map(|s| format!(".{}", s)).unwrap_or_default(),
                    dot_lang = request.lang().map(|s| format!(".{}", s)).unwrap_or_default(),
                )
            ],
            Provider::ArchLinuxAny => ["core", "extra", "community", "multilib"]
                .map(|repository|
                    format!(
                        "https://man.archlinux.org/man/{repository}/{package}/{page}{dot_section}{dot_lang}.txt",
                        package = request.package().unwrap_or(request.page()),
                        page = request.page(),
                        dot_section = request.section().map(|s| format!(".{}", s)).unwrap_or_default(),
                        dot_lang = request.lang().map(|s| format!(".{}", s)).unwrap_or_default(),
                    )
                ).to_vec(),
            Provider::FreeBSD => vec![format!(
                "https://www.freebsd.org/cgi/man.cgi?query={package}&apropos=0&sektion={section}&manpath={manpath}&arch={arch}&format=ascii",
                package = request.page(),
                section = request.section().unwrap_or("0"),
                manpath = "FreeBSD+13.1-RELEASE+and+Ports", // TODO: like archlinux 
                arch = "default", // currently, serves no purpose
            )]
        }
    }
}

fn main() {
    let opts: Opts = Opts::parse();
    let section = opts.section.get(0).unwrap();
    let page = opts.section.get(1);
}
